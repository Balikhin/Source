/*
* Main GamePlay Classes
* Add DlgSystem(Plugin/DlgSystem) for GameMode
* Add ShooterGame(Plugin/ShooterGame)
*/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ShooterGameMode.h"
#include "Online/ShooterGameMode.h"
#include "GameFramework/PlayerStart.h"

#include "OazisGameModeBase.generated.h"

UCLASS()
class AOazisGameModeBase : public AGameMode //AShooterGameMode
{
	GENERATED_BODY()
};