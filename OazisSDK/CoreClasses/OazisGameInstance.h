// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

//#include "OazisSDK.h"

#include "Logging/LogMacros.h"
#include "ShooterGame.h"
#include "OnlineIdentityInterface.h"
#include "OnlineSessionInterface.h"
#include "Engine/GameInstance.h"
#include "Engine/NetworkDelegates.h"

#include "ShooterGame/Public/ShooterGameInstance.h"
#include "Game/OazisEnums.h"
#include "Game/OazisGameMode.h"

#include "OazisGameInstance.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogOazisGameInstance, Log, All);

struct FOagtd : public FOazisGameTimeData
{
};

UCLASS(config=Game)
class OAZISSDK_API UOazisGameInstance : public UShooterGameInstance
{
public:
	GENERATED_UCLASS_BODY()
public:
	void ChangeGameMode(EOazisGameMode GameMode);
	EOazisGameMode GetOazisGameMode();

	FOagtd OazisGameTimeData; //Start DateTime1
	FOagtd GetDateTimeGame() { return OazisGameTimeData; }
	void SaveDateTimeGame(FOazisGameTimeData LastGameTimeData) 
	{ 
		OazisGameTimeData.DateTimeGame = LastGameTimeData.DateTimeGame;
		OazisGameTimeData.GameMinutes = LastGameTimeData.GameMinutes;
	}
	//bool Tick(float DeltaSeconds);
	//virtual void Init() override;
	//virtual void Shutdown() override;
	//virtual void StartGameInstance() override;
	//UPROPERTY(BlueprintReadWrite, Category = OazisSDK, meta = (AllowPrivateAccess = "true"),Config)

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OazisSDK, meta = (AllowPrivateAccess = "true"))
	EOazisGameMode OazisGameModeInstance = EOazisGameMode::NoneMode; // Default mode
private:
	virtual void BeginMainMenuState();
	
};


