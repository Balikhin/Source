// Fill out your copyright notice in the Description page of Project Settings.

#include "OazisWorldMapPawn.h"
#include "Kismet/KismetSystemLibrary.h"
#include "OazisShooterPlayerController.h"

#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

//#include "Bots/ShooterAIController.h"

static const FString MapNames[] = { TEXT("WorldMap"), TEXT("GenericMap") };

// Sets default values
AOazisWorldMapPawn::AOazisWorldMapPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationYaw = true;

	//CameraArm
	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));
	CameraArm->SetupAttachment(RootComponent);
	CameraArm->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraArm->TargetArmLength = 3000.f;
	CameraArm->RelativeRotation = FRotator(-60.0f, 0.f, 0.f);
	//CameraArm->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	CameraArm->ProbeSize = 100.f; //no inside colliders very close
	CameraArm->TargetOffset = FVector(0.f, 0.f, 100.f);
	//Camera
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(CameraArm, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	CameraComponent->bAutoActivate = false;
    
	PlayerVision = CreateDefaultSubobject<UDecalComponent>(TEXT("PlayerVision"));
	PlayerVision->SetupAttachment(RootComponent);
	PlayerVision->RelativeRotation = FRotator(90.0f, 0.f, 0.f);
	PlayerVision->DecalSize = FVector::OneVector*PlayerVisionDist;

	MoveDestinationMark = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MoveDestination"));
	//MoveDestinationMark->SetupAttachment(RootComponent);
	MovePlayerMark = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MovePlayerMark"));
	MovePlayerMark->SetupAttachment(RootComponent);
	MovePlayerMark->RelativeRotation = FRotator(90.f, 90.0f, 0.f);
}

void AOazisWorldMapPawn::OnStartUse()
{
	UKismetSystemLibrary::PrintString(GetWorld(), "Use ", 1, 1, FColor::Red, 5);
	//PossessBackToOldPawn();
}

void AOazisWorldMapPawn::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);
	FString name; GetName(name);
	//UE_LOG(LogTemp, Log, TEXT("Possessed"));
	UKismetSystemLibrary::PrintString(GetWorld(), "Possessed into " + name, 1, 1, FColor::Green, 10); // Using for Separate Client/Server side
	SetActorHiddenInGame(false);
	SetActorTickEnabled(true);
	SetActorEnableCollision(true);
}

void AOazisWorldMapPawn::UnPossessed()
{
	Super::UnPossessed();
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
	SetActorEnableCollision(false);
}

void AOazisWorldMapPawn::SetOazisPlayerPawn(APawn* PawnDriver)
{
	if (PawnDriver)
	OazisPlayerPawn = PawnDriver;
	UKismetSystemLibrary::PrintString(GetWorld(), "Set OazisPlayerPawn - " + OazisPlayerPawn->GetFullName(), 1, 1, FColor::Green, 10);
}

void AOazisWorldMapPawn::PossessBackToOldPawn()
{
	if (Role == ROLE_Authority)
	{
		AOazisShooterPlayerController* OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
		if (OazisPlayerController)
		{
			FTransform SpawnLocation = FTransform::Identity;

			SpawnLocation.SetLocation(GetActorLocation() - GetActorForwardVector()* 400);
			if (OazisPlayerPawn)
			{
				OazisPlayerPawn->SetActorTickEnabled(true);
				OazisPlayerPawn->SetActorHiddenInGame(false);
				OazisPlayerPawn->SetActorRelativeTransform(SpawnLocation);
				OazisPlayerPawn->SetActorRotation(FRotator(0, GetActorRotation().Yaw, 0), ETeleportType::ResetPhysics);
				OazisPlayerPawn->SetActorEnableCollision(true);
			}
			else return;

			OazisPlayerController->UnPossess();
			//APawn* PlayerPawn = (APawn*) GetWorld()->SpawnActor(AOazisShooterCharacter::StaticClass(),&SpawnLocation);
			OazisPlayerController->Possess(OazisPlayerPawn); //OazisPlayerPawn
		}
	}
	else
	{
		ServerPossessBackToOldPawn();
	}
}

bool AOazisWorldMapPawn::ServerPossessBackToOldPawn_Validate()
{
	return true;
}

void AOazisWorldMapPawn::ServerPossessBackToOldPawn_Implementation()
{
	PossessBackToOldPawn();
}



// Called when the game starts or when spawned
void AOazisWorldMapPawn::BeginPlay()
{
	Super::BeginPlay();
	//UActorComponent* CameraWM = GetComponentByClass(UCameraComponent::StaticClass());
	if (CameraComponent)
		CameraComponent->Activate();

	//APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), ControllerId);
	OazisGameMode = Cast<AOazisGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	
	DynamMatPlayerVision = PlayerVision->CreateDynamicMaterialInstance();
}

// Called every frame
void AOazisWorldMapPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UGameplayStatics
	if (OazisGameMode) PlayerVisionTick();
}

void AOazisWorldMapPawn::PlayerVisionTick()
{
	if (IsTravelWorldMap())
	{
		OazisGameMode->SetGameTimeSpeed(30.f);
		DynamMatPlayerVision->SetVectorParameterValue(FName("Color"), FColor::Red);
	}
	else
	{
		OazisGameMode->SetGameTimeSpeed(0.5f);
		DynamMatPlayerVision->SetVectorParameterValue(FName("Color"), FColor::Green);
	}
	MovePlayerMark->SetWorldLocation(GetLocationPlayerMouse());
	float DynamScale = UKismetMathLibrary::Clamp(UKismetMathLibrary::MapRangeUnclamped(GetLengthPlayerAndMouse(), 120.0f, 3000.0f, 0.f, 10.0f),0.f,10.0f);
	FVector MarkScale = FVector::OneVector*DynamScale;
	MovePlayerMark->SetRelativeScale3D(MarkScale);
	MovePlayerMark->AddLocalRotation(FRotator(0,0,2.f));
}

// Called to bind functionality to input
void AOazisWorldMapPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("UseWorldMap", IE_Pressed, this, &AOazisWorldMapPawn::OnStartUse);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AOazisWorldMapPawn::MovePlayerMouseDownPoint);
	//PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterCharacter::OnStopFire);
}


FVector AOazisWorldMapPawn::GetLocationPlayerMouse()
{
	AOazisShooterPlayerController* OSPC = Cast<AOazisShooterPlayerController>(Controller);
	if (OSPC)
	{
		FHitResult  HitResult;
		if (OSPC->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Camera), true, HitResult))
		{
			return HitResult.Location;
		}
	}
	return FVector::ZeroVector;
}
void AOazisWorldMapPawn::MovePlayerMouseDownPoint()
{
	if (GetLocationPlayerMouse() != FVector::ZeroVector)
	{
		AOazisShooterPlayerController* OSPC = Cast<AOazisShooterPlayerController>(Controller);
		FVector Loc = GetLocationPlayerMouse();
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(OSPC, Loc);
		//UNavigationSystem::SimpleMoveToLocation(OSPC,Loc);
		//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("You try Move"));
		MoveDestinationMark->SetWorldLocation(Loc);
	}
}

bool AOazisWorldMapPawn::IsTravelWorldMap() const
{
	if (GetCharacterMovement()->Velocity.Size2D() > 0)
		return true;
	return false;
}

float AOazisWorldMapPawn::GetLengthPlayerAndMouse()
{
	FVector PlayerLoc = GetMesh()->GetComponentLocation();
	FVector MouseLoc = GetLocationPlayerMouse();
	return (PlayerLoc - MouseLoc).Size2D();
}
