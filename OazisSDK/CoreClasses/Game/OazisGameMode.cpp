// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OazisGameMode.h"
#include "DlgManager.h"
#include "UnrealMathUtility.h"
#include "Kismet/KismetSystemLibrary.h"
#include "CoreClasses/OazisGameInstance.h"

AOazisGameMode::AOazisGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
//	DefaultPawnClass = AOazisShooterCharacter; 
	bDelayedStart = true;
}

void AOazisGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	GameTimeData.AddDateTimeGame(DeltaSeconds);
}

void AOazisGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();
	//UKismetSystemLibrary::PrintString(GetWorld(), "PreInitializeComponents()", 1, 1, FColor::Red, 6);
}

void AOazisGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	UKismetSystemLibrary::PrintString(GetWorld(), "InitGame()", 1, 1, FColor::Red, 6);
}

void AOazisGameMode::RestartGame()
{
	Super::RestartGame();
	//UKismetSystemLibrary::PrintString(GetWorld(), "RestartGame()", 1, 1, FColor::Red, 6);
}

void AOazisGameMode::StartToLeaveMap()
{
	Super::StartToLeaveMap();
	//UKismetSystemLibrary::PrintString(GetWorld(), "StartToLeaveMap()", 1, 1, FColor::Red, 6);
}

void AOazisGameMode::StartPlay()
{
	/*[SAVEGAME/LOADGAME]*/
	// Get from OazisGameInstances and CurrentGameMode and change here
	UOazisGameInstance* UOGI = Cast<UOazisGameInstance>(GetWorld()->GetGameInstance());
	if (UOGI)
	{
		if (UOGI->GetOazisGameMode() != EOazisGameMode::NoneMode)
		{
			ChangeGameMode(UOGI->GetOazisGameMode()); // If Exist get Global GamneInstance except local from GameMode
		}
		else ChangeGameMode(CurrentGameMode);
		// GameTime
		GameTimeData.GameMinutes = UOGI->GetDateTimeGame().GameMinutes;
		GameTimeData.DateTimeStartGame = UOGI->GetDateTimeGame().DateTimeGame - FTimespan(0, 0, GameTimeData.GameMinutes, 0);
	}
	// We must clear the Dialogue memory here because the FDlgMemory is a singleton and it remains in memory. Causes
	// problems in PIE and play in editor.
	UDlgManager::ClearDialogueHistory();

	//FString St = GameTimeData.DateTimeStartGame.ToString();
	//UKismetSystemLibrary::PrintString(GetWorld(), "StartPlay() GameTime = " + St, 1, 1, FColor::Red, 6);
	Super::StartPlay();
	PrimaryActorTick.bCanEverTick = true;
	
	// Register plugin console commands
	UDlgManager::RegisterDialogueModuleConsoleCommands(this);
}

void AOazisGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//UKismetSystemLibrary::PrintString(GetWorld(), "EndPlay()", 1, 1, FColor::Red, 6);
	// Unregister all the console commands
	UDlgManager::UnRegisterDialogueModuleConsoleCommands();
	
	//Save Game DateTime [SaveGame/LoadGame]
	UOazisGameInstance* UOGM = Cast<UOazisGameInstance>(GetWorld()->GetGameInstance());
	if (UOGM) UOGM->SaveDateTimeGame(GameTimeData);
	FString St = GameTimeData.DateTimeGame.ToString();
	UKismetSystemLibrary::PrintString(GetWorld(), "EndPlay() GameTime = " + St, 1, 1, FColor::Red, 6);
}

void AOazisGameMode::DetermineMatchWinner()
{
	AShooterGameState const* const MyGameState = CastChecked<AShooterGameState>(GameState);
	float BestScore = MAX_FLT;
	int32 BestPlayer = -1;
	int32 NumBestPlayers = 0;

	for (int32 i = 0; i < MyGameState->PlayerArray.Num(); i++)
	{
		const float PlayerScore = MyGameState->PlayerArray[i]->Score;
		if (BestScore < PlayerScore)
		{
			BestScore = PlayerScore;
			BestPlayer = i;
			NumBestPlayers = 1;
		}
		else if (BestScore == PlayerScore)
		{
			NumBestPlayers++;
		}
	}

	WinnerPlayerState = (NumBestPlayers == 1) ? Cast<AShooterPlayerState>(MyGameState->PlayerArray[BestPlayer]) : NULL;
}


bool AOazisGameMode::IsWinner(AShooterPlayerState* PlayerState) const
{
	return PlayerState && !PlayerState->IsQuitter() && PlayerState == WinnerPlayerState;
}

void AOazisGameMode::ChangeGameMode(EOazisGameMode GameMode)
{
	FString GModeStr="None";
	if (GameMode == EOazisGameMode::WorldMapMode)
	{
		GModeStr = "WorldMapMode";
		DefaultPawnClass = WorldMapPawnClass;
		CurrentGameMode = GameMode;
		Cast<AOazisShooterHUD>(GetWorld()->GetFirstPlayerController()->GetHUD())->CurrentGameMode = CurrentGameMode;
	}
	else
	{
		if (GameMode == EOazisGameMode::ShooterMode)
		{
			GModeStr = "ShooterMode";
			DefaultPawnClass = ShooterPawnClass;
			CurrentGameMode = GameMode;
			Cast<AOazisShooterHUD>(GetWorld()->GetFirstPlayerController()->GetHUD())->CurrentGameMode = CurrentGameMode;
		}
	}
	UKismetSystemLibrary::PrintString(GetWorld(), "ChangeGameMode - " + GModeStr, 1, 1, FColor::Red, 6);
}

EOazisGameMode AOazisGameMode::GetCurrentMode()
{
	return CurrentGameMode;
}

void AOazisGameMode::SetGameTimeSpeed(float timespeed)
{
	GameTimeData.SetTimeSpeed(timespeed);
}

TSubclassOf<APawn> AOazisGameMode::GetPawnClassGameMode(EOazisGameMode GameMode)
{
	switch (GameMode)
	{
	case EOazisGameMode::ShooterMode:
		return ShooterPawnClass;
	case EOazisGameMode::WorldMapMode:
		return WorldMapPawnClass;

	default:
		return NULL;
	}
}

