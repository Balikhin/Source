// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "OazisShooterCharacter.h"


#include "OazisGliderPawn.generated.h"

UCLASS()
class OAZISSDK_API AOazisGliderPawn : public APawn
{
	GENERATED_BODY()
	/** [server] perform PlayerState related setup */
	virtual void PossessedBy(class AController* C) override;
public:
	// Sets default values for this pawn's properties
	AOazisGliderPawn();
	void OnStartUse();
	/**
	* [server + local] Possess new Pawn
	*
	* @param NewPawn Pawn to possess
	*/
	void PossessBackToOldPawn();
	/** Posses new pawn */
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerPossessBackToOldPawn();

	void SetOazisPlayerPawn(APawn* PawnDriver);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Glider")
	bool bCanFly;

	virtual void UnPossessed();
protected:
	APawn* OazisPlayerPawn = nullptr; //Main Player Character use HangGlider
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
