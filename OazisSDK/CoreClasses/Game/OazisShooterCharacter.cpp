﻿// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OazisShooterCharacter.h"
#include "OazisShooterPlayerController.h"
#include "Bots/ShooterBot.h"
#include "OazisShooterCharacter.h"
#include "Bots/ShooterAIController.h"
#include "ShooterWeapon.h"
#include "OazisBuggyPawn.h"
#include "OazisGliderPawn.h"

AOazisShooterCharacter::AOazisShooterCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

/** Dialogue System interface */
bool AOazisShooterCharacter::ModifyIntValue_Implementation(const FName& ValueName, bool bDelta, int32 Value)
{
	if (!DlgData.Integers.Contains(ValueName))
		DlgData.Integers.Add(ValueName, 0);

	if (bDelta)
		DlgData.Integers[ValueName] += Value;
	else
		DlgData.Integers[ValueName] = Value;

	return true;
}

bool AOazisShooterCharacter::ModifyFloatValue_Implementation(const FName& ValueName, bool bDelta, float Value)
{
	if (!DlgData.Floats.Contains(ValueName))
		DlgData.Floats.Add(ValueName, 0.0f);

	if (bDelta)
		DlgData.Floats[ValueName] += Value;
	else
		DlgData.Floats[ValueName] = Value;

	return true;
}

bool AOazisShooterCharacter::ModifyBoolValue_Implementation(const FName& ValueName, bool bValue)
{
	if (bValue)
		DlgData.TrueBools.Add(ValueName);
	else
		DlgData.TrueBools.Remove(ValueName);

	return true;
}

bool AOazisShooterCharacter::ModifyNameValue_Implementation(const FName& ValueName, const FName& NameValue)
{
	if (DlgData.Names.Contains(ValueName))
		DlgData.Names[ValueName] = NameValue;
	else
		DlgData.Names.Add(ValueName, NameValue);

	return true;
}


float AOazisShooterCharacter::GetFloatValue_Implementation(const FName& ValueName) const
{
	return DlgData.Floats.Contains(ValueName) ? DlgData.Floats[ValueName] : 0.0f;
}

int32 AOazisShooterCharacter::GetIntValue_Implementation(const FName& ValueName) const
{
	return DlgData.Integers.Contains(ValueName) ? DlgData.Integers[ValueName] : 0;
}

bool AOazisShooterCharacter::GetBoolValue_Implementation(const FName& ValueName) const
{
	return DlgData.TrueBools.Contains(ValueName);
}

FName AOazisShooterCharacter::GetNameValue_Implementation(const FName& ValueName) const
{
	return DlgData.Names.Contains(ValueName) ? DlgData.Names[ValueName] : NAME_None;
}

void AOazisShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	//PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AOazisShooterCharacter::OnStartTalk);
	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AOazisShooterCharacter::OnStartInteraction);
}

void AOazisShooterCharacter::OnStartInteraction()
{
	if (Controller)
	{
		AOazisShooterPlayerController* OazisPlayerController = NULL;
		OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
		if (OazisPlayerController->ActorForTalk) OnStartTalk(); else
		if (OazisPlayerController->ActorForUse) OnStartUse();		
	}
}

void AOazisShooterCharacter::OnStartTalk()
{
	AOazisShooterPlayerController* OazisPlayerController = NULL;
	if (Controller)
	{
		OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
		AActor* TalkActor = OazisPlayerController->ActorForTalk;
		AOazisShooterCharacter* TalkNPCPawn = Cast<AOazisShooterCharacter>(TalkActor);
		FString sNpcName;
		if (TalkNPCPawn)
		{
			TalkNPCPawn->GetName(sNpcName);
			if (GEngine) 
				GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("You try start Talk - " + sNpcName));
			if (TalkNPCPawn->Dialog)
			{
				OazisPlayerController->StartDialogue(TalkNPCPawn->Dialog, TalkNPCPawn);
				if (wWidgetDialog)
				{
					DialogWidget = CreateWidget<UUserWidget>(OazisPlayerController, wWidgetDialog);
					DialogWidget->AddToViewport();
				}
			}
			else
			{
				if (GEngine) 
					GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("NPC - " + sNpcName + " dont have dialogue."));
			}
		}
	}
}

void AOazisShooterCharacter::OnStartUse()
{
	AOazisShooterPlayerController* OazisPlayerController = NULL;
	if (Controller)
	{
		OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
		AActor* UseActor = OazisPlayerController->ActorForUse;
		AOazisBuggyPawn* OazisBuggyPawn = Cast<AOazisBuggyPawn>(UseActor);
		if (OazisBuggyPawn)
		{
			PossessNewPawn(OazisBuggyPawn);
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("You try use Vehicle - Buggy "));
		}
		AOazisShooterCharacter* OazisShooterPawn = Cast<AOazisShooterCharacter>(UseActor);
		if (OazisShooterPawn)
		{
			PossessNewPawn(OazisShooterPawn);
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("You try possess Pawn -  ShooterPawn"));
		}
		AOazisGliderPawn* OazisGliderPawn = Cast<AOazisGliderPawn>(UseActor);
		if (OazisGliderPawn)
		{
			PossessNewPawn(OazisGliderPawn);
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("You try use Vehicle - Glider"));
		}
	}
}

void AOazisShooterCharacter::UnPossessed()
{
	Super::UnPossessed();
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
	SetActorEnableCollision(false);
	if (CurrentWeapon) CurrentWeapon->SetActorHiddenInGame(true);
}

void AOazisShooterCharacter::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);
	SetActorHiddenInGame(false);
	SetActorTickEnabled(true);
	SetActorEnableCollision(true);
	if (CurrentWeapon) CurrentWeapon->SetActorHiddenInGame(false);
	FString name; GetName(name);
	UKismetSystemLibrary::PrintString(GetWorld(), "Possessed into " + name, 1, 1, FColor::Green, 3);
}

void AOazisShooterCharacter::PossessNewPawn(APawn* NewPawn)
{
	AOazisGliderPawn* OazisGliderPawn = Cast<AOazisGliderPawn>(NewPawn);
	AOazisBuggyPawn* OazisBuggyPawn = Cast<AOazisBuggyPawn>(NewPawn);
	if (NewPawn)
	{
		if (Role == ROLE_Authority)
		{
			AOazisShooterPlayerController* OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
			if (OazisPlayerController)
			{
				if (OazisBuggyPawn) OazisBuggyPawn->SetOazisPlayerPawn(this);
				if (OazisGliderPawn) OazisGliderPawn->SetOazisPlayerPawn(this);
				//Suicide(); // for normal netupdate
				//SetActorHiddenInGame(true);
				//SetActorTickEnabled(false);
				//SetActorEnableCollision(false);
				//AttachToActor(OazisBuggyPawn,FAttachmentTransformRules::KeepRelativeTransform); //KeepRelativeTransform
				//OazisPlayerController->UnPossess();
				OazisPlayerController->Possess(NewPawn);
				NewPawn->SetReplicateMovement(true);
				OazisPlayerController->SetPawn(NewPawn);
				OazisPlayerController->ServerRestartPlayer();
				NewPawn->SetOwner(OazisPlayerController);
				//this->Destroy(); //only spawn
			}
		}
		else
		{
			if (OazisBuggyPawn) 
			{
				//OazisBuggyPawn->EngineAC->Play();
				UKismetSystemLibrary::PrintString(GetWorld(), "Start Engine", 1, 1, FColor::Green, 3);
			}
			ServerPossessNewPawn(NewPawn);
		}
	}
}

bool AOazisShooterCharacter::ServerPossessNewPawn_Validate(APawn* NewPawn)
{
	return true;
}

void AOazisShooterCharacter::ServerPossessNewPawn_Implementation(APawn* NewPawn)
{
	PossessNewPawn(NewPawn);
}

void AOazisShooterCharacter::Suicide()
{
	Super::Suicide();
}