// Fill out your copyright notice in the Description page of Project Settings.
//Build process https://docs.unrealengine.com/en-us/Programming/BuildTools

using UnrealBuildTool;
using System.IO;

public class OazisSDK : ModuleRules
{
    public OazisSDK(ReadOnlyTargetRules Target) : base(Target)
	{
        // Enable IWYU
        // https://docs.unrealengine.com/latest/INT/Programming/UnrealBuildSystem/IWYUReferenceGuide/index.html
        // https://docs.unrealengine.com/latest/INT/Programming/UnrealBuildSystem/Configuration/
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = true;
        PrivateIncludePathModuleNames.AddRange(
            new string[] {
                "NetworkReplayStreaming",
                "DlgSystem",
                "ShooterGame",
                "VehicleGame"
        });
        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "OnlineSubsystem",
            "OnlineSubsystemUtils",
            "AssetRegistry",
            "NavigationSystem",
            "AIModule",
            "GameplayTasks",
            "DlgSystem", //start use modules for OazisSDK
            "ShooterGame",
            "VehicleGame",
            "PhysXVehicles" //duplicate module from plugin VehicleGame, if you try use plugin classes but not listed module from plugin here get lnk errors.
        });

		PrivateDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "InputCore",
            "Slate",
            "SlateCore",
            "ShooterGameLoadingScreen",
            "Json",
            "ApplicationCore",
            "ReplicationGraph",
            "DlgSystem",
            "ShooterGame",
            "UMG", "Slate", "SlateCore"
        });
        /*
        string ProjectParentDir = System.IO.Directory.GetParent(Target.ProjectFile.ToString()).ToString(); // OazisSDK
        string ProjectSourcePath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath); // OazisSDK/Source
        string PluginsPath = Path.Combine(ProjectParentDir, "Plugins"); //Plugins of OazisSDK
        string PluginShooterGamePath = Path.Combine(PluginsPath, "ShooterGame/Source/ShooterGame"); //Source of plugin ShooterGame
            PrivateIncludePaths.AddRange(
                    new string[] {
                    Path.Combine(PluginShooterGamePath, "Private"),
                    Path.Combine(PluginShooterGamePath, "Private/Online"),
                    Path.Combine(PluginShooterGamePath, "Private/UI"),
                    Path.Combine(PluginShooterGamePath, "Private/UI/Menu"),
                    Path.Combine(PluginShooterGamePath, "Private/UI/Style"),
                    Path.Combine(PluginShooterGamePath, "Private/UI/Widgets"),
                    });
            PublicIncludePaths.AddRange(
                  new string[] {
                    Path.Combine(PluginShooterGamePath, "Public"),
                    Path.Combine(PluginShooterGamePath, "Public/Online"),
                  });
                  */
        // Uncomment if you are using Slate UI

        //PrivateDependencyModuleNames.AddRange(new string[] { "UMG", "Slate", "SlateCore" });

            // Uncomment if you are using online features
            // PrivateDependencyModuleNames.Add("OnlineSubsystem");

            // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
        }
}
