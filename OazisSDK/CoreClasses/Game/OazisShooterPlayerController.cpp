// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OazisShooterPlayerController.h"
#include "CoreClasses/OazisGameInstance.h"
#include "Kismet/GameplayStatics.h"


AOazisShooterPlayerController::AOazisShooterPlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerCameraManagerClass = AShooterPlayerCameraManager::StaticClass();
	CheatClass = UShooterCheatManager::StaticClass();
	bAllowGameActions = true;
	bGameEndedFrame = false;
	LastDeathLocation = FVector::ZeroVector;

	ServerSayString = TEXT("Say");
	ShooterFriendUpdateTimer = 0.0f;
	bHasSentStartEvents = false;

	StatMatchesPlayed = 0;
	StatKills = 0;
	StatDeaths = 0;
	bHasFetchedPlatformData = false;
}

//Input Additional
void AOazisShooterPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	// UI input
	InputComponent->BindAction("UseWorldMap", IE_Pressed, this, &AOazisShooterPlayerController::WorldMapToShooterMap);
}

void AOazisShooterPlayerController::WorldMapToShooterMap()
{
	// change OazisGameInstances CurrentGameMode 
	UOazisGameInstance* UOGM = Cast<UOazisGameInstance>(GetWorld()->GetGameInstance());
	UOGM->ChangeGameMode(EOazisGameMode::ShooterMode);
	UGameplayStatics::OpenLevel(GetWorld(), FName("GenericMap"), true);
}

void AOazisShooterPlayerController::WorldMapToShooterMap(FName LocalMap)
{
	// change OazisGameInstances CurrentGameMode 
	UOazisGameInstance* UOGM = Cast<UOazisGameInstance>(GetWorld()->GetGameInstance());
	UOGM->ChangeGameMode(EOazisGameMode::ShooterMode);
	UGameplayStatics::OpenLevel(GetWorld(), LocalMap, true);
}

void AOazisShooterPlayerController::ExitToWorldMap()
{
	// change OazisGameInstances CurrentGameMode 
	UOazisGameInstance* UOGM = Cast<UOazisGameInstance>(GetWorld()->GetGameInstance());
	UOGM->ChangeGameMode(EOazisGameMode::WorldMapMode);
	UGameplayStatics::OpenLevel(GetWorld(), FName("WorldMap"), true);
}

void AOazisShooterPlayerController::BeginPlaySpawnNewPawn()
{
	UKismetSystemLibrary::PrintString(GetWorld(), "BeginPlaySpawnNewPawn()", 1, 1, FColor::Red, 6);
	AOazisGameMode* GM = GetOazisGameMode();
	FTransform SpawnLocation = FTransform::Identity;
	SpawnLocation.SetLocation(GetPawn()->GetActorLocation() + FVector(200, 200, 300));
	if (GM->GetCurrentMode() == EOazisGameMode::ShooterMode)
	{
		if (GetPawn()->StaticClass() != GM->GetPawnClassGameMode(EOazisGameMode::ShooterMode))
		{
			if (!OazisComebackPawn)
			{
				APawn* PlayerPawn = (APawn*)GetWorld()->SpawnActor(GM->GetPawnClassGameMode(EOazisGameMode::ShooterMode), &SpawnLocation); //AOazisWorldMapPawn::StaticClass()
				if (PlayerPawn) PossessNewPawn(PlayerPawn);
			}
			else
			{
				PossessNewPawn(OazisComebackPawn);
			}
		}
		//bShowMouseCursor = true;
	}
	else
	{
		if (GM->GetCurrentMode() == EOazisGameMode::WorldMapMode)
		{
			if (GetPawn()->StaticClass() != GM->GetPawnClassGameMode(EOazisGameMode::ShooterMode))
			{
				//GM->ChangeGameMode(EOazisGameMode::ShooterMode);
				if (!OazisComebackPawn)
				{
					APawn* PlayerPawn = (APawn*)GetWorld()->SpawnActor(GM->GetPawnClassGameMode(EOazisGameMode::WorldMapMode), &SpawnLocation);
					if (PlayerPawn) PossessNewPawn(PlayerPawn);
				}
				else
				{
					PossessNewPawn(OazisComebackPawn);
				}
			}
			//bShowMouseCursor = false;
		}
	}
}

//DlgSystem
void AOazisShooterPlayerController::StartDialogue(class UDlgDialogue* Dialogue, UObject* OtherParticipant)
{
	ActiveContext = UDlgManager::StartDialogue2(Dialogue, GetPawn(), OtherParticipant);

	// Make mouse visible and control dialogue widget
	if (GetPawn()) GetPawn()->DisableInput(this);
	FInputModeGameAndUI InputMode;
	SetInputMode(InputMode);
	bShowMouseCursor = true;

}

void AOazisShooterPlayerController::SelectDialogueOption(int32 Index)
{
	if (ActiveContext == nullptr || Index < 0 || Index >= ActiveContext->GetOptionNum())
		return;

    /**/// Dialog End! Finished and Focus disabled for mouse
	if (!ActiveContext->ChooseChild(Index))
	{
		ActiveContext = nullptr;
		
		// Make mouse hide and close dialogue
		if (GetPawn()) GetPawn()->EnableInput(this);
		bShowMouseCursor = false;
		FInputModeGameOnly InputMode;
		SetInputMode(InputMode);

		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Dialog Finished. "));
	}
}

void AOazisShooterPlayerController::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	SetActorForInterActive(ActorInPlayerCamera()); // Get Actor visible in current Camera
}

AActor* AOazisShooterPlayerController::ActorInPlayerCamera()
{
	float VisionOffset = 400.f;
	FHitResult HitResult(ForceInit);
	/**
	*  Trace a ray against the world using a specific channel and return the first blocking hit
	*  @param  OutHit          First blocking hit found
	*  @param  Start           Start location of the ray
	*  @param  End             End location of the ray
	*  @param  TraceChannel    The 'channel' that this ray is in, used to determine which components to hit
	*  @param  Params          Additional parameters used for the trace
	* 	@param 	ResponseParam	ResponseContainer to be used for this trace
	*  @return TRUE if a blocking hit is found
	*/
	if (GetPawn())
	{
		FVector CamLoc;
		FRotator CamRot;
		GetPlayerViewPoint(CamLoc, CamRot);
		// Perform trace to retrieve hit info
		FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(ActorInPlayerCamera), true, GetPawn()); //Instigator
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;
		FVector VisionLocation;
		FVector StartLocation = CamLoc;// GetPawn()->GetActorLocation();//+ CameraDir.Vector()*400.f;
		VisionLocation = StartLocation + CamRot.Vector() * VisionOffset;
		bool bBlocked;
		bBlocked = GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, VisionLocation, ECC_Camera, TraceParams); //ECC_Camera ECC_Pawn
		if (!bBlocked)
		{
			return nullptr;
		}
	}
	if (HitResult.GetActor())
	{
			return HitResult.GetActor();
	}
	return nullptr;
}

void AOazisShooterPlayerController::SetActorForInterActive(AActor* refActor)
{
	if (!refActor)
	{
		SetActorForTalk(nullptr);
		SetActorForUse(nullptr);
		return;
	}
	FString ActorName;
	refActor->GetName(ActorName);
	// Be sure it is a participant
	if (UDlgManager::DoesObjectImplementDialogueParticipantInterface(refActor))
	{
		if (Cast<AShooterCharacter>(refActor))
		{
			if ((Cast<AShooterCharacter>(refActor))->IsAlive())
				SetActorForTalk(refActor);
			else
			{
				SetActorForTalk(nullptr);
				//check maybe can use dead object
			}
		};
		SetActorForUse(refActor);
		if (GEngine) 
			if (ActorForUse) GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Yellow, TEXT("You see NPC " + ActorName+" [Dialogue & Use]"));
			else GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Yellow, TEXT("You see NPC " + ActorName + " [Dialogue]"));
		return;
	}
	else
	{
		SetActorForUse(refActor);
		SetActorForTalk(nullptr);
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Yellow, TEXT("You see " + ActorName+" [Use]"));
	}
}

AOazisShooterHUD* AOazisShooterPlayerController::GetOazisShooterHUD() const
{
	return Cast<AOazisShooterHUD>(GetHUD());
}

void AOazisShooterPlayerController::SetActorForUse(AActor* refActor)
{
	ActorForUse = refActor;
}

void AOazisShooterPlayerController::SetActorForTalk(AActor* refActor)
{
	ActorForTalk = refActor;
}

void AOazisShooterPlayerController::PossessNewPawn(APawn* NewPawn)
{
	if (NewPawn)
	{
	AOazisWorldMapPawn* OazisWorldPawn = Cast<AOazisWorldMapPawn>(NewPawn);
	AShooterCharacter* OazisShooterPawn = Cast<AShooterCharacter>(NewPawn);
	AOazisGliderPawn* OazisGliderPawn = Cast<AOazisGliderPawn>(NewPawn);
	AOazisBuggyPawn* OazisBuggyPawn = Cast<AOazisBuggyPawn>(NewPawn);
		SetOazisComebackPawn(GetPawn());
		if (Role == ROLE_Authority)
		{
				Possess(NewPawn);
				//NewPawn->SetReplicateMovement(true);
				//SetPawn(NewPawn);
				ServerRestartPlayer();
				//NewPawn->SetOwner(GetPawn());
		}
		else
		{
			ServerPossessNewPawn(NewPawn);
		}
	}
}

void AOazisShooterPlayerController::SetOazisComebackPawn(APawn* OldPawn)
{
	if (OldPawn)
	{
		OazisComebackPawn = OldPawn;
		UKismetSystemLibrary::PrintString(GetWorld(), "Set ComebackPawn - " + OldPawn->GetFullName(), 1, 1, FColor::Green, 5);
	}
}

bool AOazisShooterPlayerController::ServerPossessNewPawn_Validate(APawn* NewPawn)
{
	return true;
}

void AOazisShooterPlayerController::ServerPossessNewPawn_Implementation(APawn* NewPawn)
{
	PossessNewPawn(NewPawn);
}

AOazisGameMode* AOazisShooterPlayerController::GetOazisGameMode()
{
	if (GetWorld())
		return Cast<AOazisGameMode>(GetWorld()->GetAuthGameMode());
	else return nullptr;
}

void AOazisShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	//BeginPlaySpawnNewPawn(); //Change game Pawn When StartGamePlay later need refactor
}