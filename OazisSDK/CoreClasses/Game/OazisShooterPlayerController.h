// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Online.h"
#include "CoreClasses/UI/OazisShooterHUD.h"
#include "Player/ShooterPlayerController.h"
#include "Player/ShooterPlayerCameraManager.h"
#include "Player/ShooterCharacter.h"
#include "Player/ShooterCheatManager.h"
#include "UI/ShooterHUD.h"
#include "DlgContext.h"
#include "DlgManager.h"

#include "OazisBuggyPawn.h" //posses
#include "OazisGliderPawn.h"
#include "OazisWorldMapPawn.h"
#include "OazisGameMode.h"

#include "OazisEnums.h"
#include "OazisShooterPlayerController.generated.h"

UCLASS(config=Game)
class OAZISSDK_API AOazisShooterPlayerController : public AShooterPlayerController
{
	GENERATED_UCLASS_BODY()
public:
	AOazisGameMode* GetOazisGameMode();
	//Game Mode control
	void BeginPlaySpawnNewPawn();
	void WorldMapToShooterMap();
	void WorldMapToShooterMap(FName LocalMap);

	UFUNCTION(BlueprintCallable, Category = OazisSDK)
	void ExitToWorldMap();


	void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = DlgSystem)
	void StartDialogue(class UDlgDialogue* Dialogue, UObject* OtherParticipant);

	UFUNCTION(BlueprintCallable, Category = DlgSystem)
	void SelectDialogueOption(int32 Index);
	/** Returns a pointer to the Oazis shooter game hud. May return NULL. */
	AOazisShooterHUD* GetOazisShooterHUD() const;

	//Look Actor in Player Eye
	UFUNCTION(BlueprintCallable, Category = OazisSDK)
	AActor* ActorInPlayerCamera();
	//Interactive with Actors Oazis
	void SetActorForInterActive(AActor* refActor);
	void SetActorForUse(AActor* refActor);
	void SetActorForTalk(AActor* refActor);
	/** Reference Actor used to get the UWorld. */
	UPROPERTY(EditAnywhere, Category = OazisSDK)
	AActor* ActorForUse = nullptr;
	/** Reference Actor used to get the UWorld. */
	UPROPERTY(EditAnywhere, Category = OazisSDK)
	AActor* ActorForTalk = nullptr;

	void PossessNewPawn(APawn* NewPawn);
	UFUNCTION(Reliable, Server, WithValidation) //reliable
	void ServerPossessNewPawn(class APawn* NewPawn);
	void SetOazisComebackPawn(APawn* OldPawn);
protected:
	int32 Days;
	int32 Minutes;

	APawn* OazisComebackPawn = nullptr; //Pawn for comeback Possess
	// Begin PlayerController interface
	//virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
	/** Overridable native event for when play begins for this actor. */
	virtual void BeginPlay();
protected:
	UPROPERTY(BlueprintReadOnly)
		class UDlgContext* ActiveContext = nullptr;
};