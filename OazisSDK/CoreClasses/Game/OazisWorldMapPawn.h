// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "OazisShooterCharacter.h"
#include "OazisGameMode.h"

#include "ShooterCharacter.h"

#include "OazisWorldMapPawn.generated.h"

UCLASS()
class OAZISSDK_API AOazisWorldMapPawn : public ACharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this pawn's properties
	AOazisWorldMapPawn();
	void OnStartUse();

	AOazisGameMode* OazisGameMode;
	UFUNCTION(BlueprintCallable, Category = "OazisSDK | WorldMap Pawn")
	bool IsTravelWorldMap() const;
	float GetLengthPlayerAndMouse();
	FVector GetLocationPlayerMouse();
	void MovePlayerMouseDownPoint(); // Simple Move WorldPawn at point click

	/** [server + local] Possess new Pawn
	* @param NewPawn Pawn to possess*/
	void PossessBackToOldPawn();
	/** Posses new pawn */
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerPossessBackToOldPawn();

	void SetOazisPlayerPawn(APawn* PawnDriver);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OazisSDK | WorldMap Pawn")
	float PlayerVisionDist = 2048.0f;

	/** Called when our Controller no longer possesses us. */
	virtual void UnPossessed() override;
	/** [server] perform PlayerState related setup */
	virtual void PossessedBy(class AController* C) override;
protected:
	APawn* OazisPlayerPawn = nullptr; //Main Player Character use HangGlider
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MoveDestinationMark;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MovePlayerMark;
	/** Top down WorldMap camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent;
	/** Camera Arm positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraArm;
	/** Top down WorldMap camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* PlayerVision;

	class UMaterialInstanceDynamic* DynamMatPlayerVision;

	void PlayerVisionTick(); //Tick Vision Decal Color in Tick
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
