// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Canvas.h"
#include "UI/ShooterHUD.h"
#include "ShooterTypes.h"
#include "CoreClasses/Game/OazisEnums.h"
#include "SlateBasics.h"
#include "SlateExtras.h"

#include "SlateGameResources.h"
#include "OazisShooterHUD.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogOazisHUD, Log, All);

//Worldmap additionals for shooter HUD
class STestBtn : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STestBtn)
    //:_Set_EOG(EOazisGameMode::NoneMode)
	{}
	//SLATE_ARGUMENT(EOazisGameMode, Set_EOG)
	SLATE_END_ARGS()

		void Construct(const FArguments& InArgs);

	/** pointer to our owner PC */
	//ULocalPlayer* PlayerOwner;
	//APlayerController* PC;
private:

	EVisibility GetVisibility(EOazisGameMode EOG) const
	{
		if (EOG == EOazisGameMode::WorldMapMode) return EVisibility::Visible;
		return EVisibility::Hidden;
	}

	/** loading screen image brush */
	TSharedPtr<FSlateDynamicImageBrush> STestBtnBrush;
};


UCLASS()
class OAZISSDK_API AOazisShooterHUD : public AShooterHUD //AHUD
{
	GENERATED_UCLASS_BODY()

public:
	/** Main HUD update loop. */
	virtual void DrawHUD() override;
	//some MenuAdd
	void BuildRightPanel();
	TSharedPtr<STestBtn> TestBtnWidget;

	EOazisGameMode CurrentGameMode;
	/**If Player see something for Use*/
	void DrawTalkMessage();
	/*Debug string HUD*/
	void DrawOazisDebugInfoString(const FString& Text, float PosX, float PosY, bool bAlignLeft, bool bAlignTop, const FColor& TextColor);
	//Dates
	FString OazisDateTime(FDateTime Date);

	virtual void Destroyed();
	void ShowWorldMapWidget(bool show);
protected:
	/** Overridable native event for when play begins for this actor. */
	virtual void BeginPlay();
	/** Oazis Dialog icon for UseMessage. */
	UPROPERTY()
		FCanvasIcon DialogIcon;

	/** Oazis Texture for dlg indicator. */
	UPROPERTY()
		UTexture2D* DialogNotifyTexture;

private:
};

