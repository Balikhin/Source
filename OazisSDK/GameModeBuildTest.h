#pragma once

#include "CoreMinimal.h"
#include "ShooterGame/Public/Online/ShooterGame_Menu.h"
#include "ShooterGame.h"
#include "ShooterGame/Private/UI/Menu/ShooterMainMenu.h"
#include "ShooterGame/Private/UI/Menu/ShooterWelcomeMenu.h"
#include "ShooterGame/Private/UI/Menu/ShooterMessageMenu.h"
#include "ShooterGame/Public/Player/ShooterPlayerController_Menu.h"
#include "Online/ShooterGameSession.h"
#include "GameModeBuildTest.generated.h"

UCLASS()
class AGameModeBuildTest : public AGameModeBase //+ShooterGame // Build Mixed GameMode for test compatible Plugins Modules/API
{
	GENERATED_BODY()
};