// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OazisShooterHUD.h"
#include "CoreClasses/Game/OazisShooterPlayerController.h"
#include "CoreClasses/Game/OazisShooterCharacter.h"
#include "ShooterGame.h"
#include "Private/UI/Widgets/SShooterScoreboardWidget.h"
#include "Private/UI/Widgets/SChatWidget.h"
#include "Engine/ViewportSplitScreen.h"
#include "Weapons/ShooterWeapon.h"
#include "Weapons/ShooterDamageType.h"
#include "Weapons/ShooterWeapon_Instant.h"
#include "Online/ShooterPlayerState.h"
#include "Misc/NetworkVersion.h"

#include "ShooterGame/Private/UI/Menu/Widgets/SShooterMenuItem.h"
#include "ShooterGame/Private/UI/Menu/Widgets/SShooterMenuWidget.h"
#include "ShooterGame/Public/ShooterGameViewportClient.h"
#include "ShooterGame/Public/ShooterGameInstance.h"
#include "Engine/GameInstance.h"
#include "ShooterGame/Private/UI/Style/ShooterStyle.h"

DEFINE_LOG_CATEGORY(LogOazisHUD);

#define LOCTEXT_NAMESPACE "OazisGame.HUD.Menu"

AOazisShooterHUD::AOazisShooterHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UTexture2D> DialogNotifyTextureOb(TEXT("/ShooterGame/UI/HUD/dialogue3")); // Oazis sdk +DLG
																													   // Oazis +DLG load texture and icon
	DialogNotifyTexture = DialogNotifyTextureOb.Object;
	DialogIcon = UCanvas::MakeIcon(DialogNotifyTexture, 0, 0, 512, 512);
	
	float Offset = 20.0f;
	HUDLight = FColor(175, 202, 213, 255);
	HUDDark = FColor(110, 124, 131, 255);
	ShadowedFont.bEnableShadow = true;
}

void AOazisShooterHUD::DrawHUD()
{
	if (Canvas == nullptr)
	{
		return;
	}
	float ScaleUI = Canvas->ClipY / 1080.0f;
	AOazisShooterPlayerController* MyOazisPC = Cast<AOazisShooterPlayerController>(PlayerOwner);
	switch (CurrentGameMode)
	{
		case EOazisGameMode::ShooterMode:
		{	
			Super::DrawHUD();
			// net mode
			if (GetNetMode() != NM_Standalone)
			{
				FVector2D TextLen;
				Canvas->StrLen(NormalFont, "Client", TextLen.X, TextLen.Y);
				DrawDebugInfoString(FString::Printf(TEXT(" Oazis SDK ")), Canvas->OrgX + Offset * ScaleUI + TextLen.X, Canvas->OrgY + 5 * Offset*ScaleUI, true, true, HUDLight);
			}
			// Draw Use Message Mechanic Game[Use Items, Dialogs, Interactive]
			if (MatchState == EShooterMatchState::Playing)
			{
				AOazisShooterCharacter* MyOazisPawn = Cast<AOazisShooterCharacter>(GetOwningPawn());
				if (MyOazisPC)
					if (MyOazisPawn && MyOazisPawn->IsAlive()) //Here check only player pawn
					{
						if (MyOazisPC->ActorForTalk || MyOazisPC->ActorForUse) DrawTalkMessage();
					}
			}
			//Oazis HUD
			FDateTime DTG = MyOazisPC->GetOazisGameMode()->GetDateTimeGame();
			FString OazisDate = OazisDateTime(DTG);
			FString Text = FString::Printf(TEXT("Shooter Mode \n Game Date : ")) + OazisDate;

			DrawOazisDebugInfoString(Text, 100.f, 100.f, true, true, HUDLight);
			break;
		}
	    case EOazisGameMode::WorldMapMode:
		{
			FDateTime DTG = MyOazisPC->GetOazisGameMode()->GetDateTimeGame();
			FString OazisDate = OazisDateTime(DTG);
			FString Text = FString::Printf(TEXT("WorldMap Mode \n Game Date : ")) + OazisDate;

			DrawOazisDebugInfoString(Text, 100.f, 100.f, true, true, HUDLight);


			//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("Game Date "+ Text));
			//Test Localization and Positions and Concatenate
			FText WorldMapText = LOCTEXT("WorldMap", "WORLD MAP");
			FVector2D TextSize;
			Canvas->StrLen(NormalFont, WorldMapText.ToString(), TextSize.X, TextSize.Y);
			const float PosX = (Canvas->ClipX - TextSize.X * ScaleUI) / 2;
			const float PosY = Canvas->OrgY + TextSize.Y * ScaleUI;
			FCanvasTextItem TextItem1(FVector2D::ZeroVector, WorldMapText, NormalFont, HUDDark);
			TextItem1.EnableShadow(FLinearColor::Black);
			TextItem1.FontRenderInfo = ShadowedFont;
			Canvas->DrawItem(TextItem1, PosX, PosY);
			break;
		}
		case EOazisGameMode::NoneMode:
		{
			DrawOazisDebugInfoString(FString::Printf(TEXT("NoneMode")), 100.f, 100.f, true, true, HUDLight);
			break;
		}
	    default:
		{
			DrawOazisDebugInfoString(FString::Printf(TEXT("Default Mode")), 100.f, 100.f, true, true, HUDLight);
			break;
		}
	}
}

/* Oazis SDK                          */
void  AOazisShooterHUD::DrawTalkMessage()
{
	if (PlayerOwner == NULL)
	{
		return;
	}
	float OffsetX = 80;
	float OffsetY = 20;
	Canvas->SetDrawColor(FColor::Orange);

	const float UseMessagePosX = (Canvas->ClipX - HealthBarBg.UL * ScaleUI) / 2;
	const float UseMessagePosY = Canvas->OrgY + OffsetY * ScaleUI;
	FVector Scale(ScaleUI, ScaleUI, 0.f);
	// hardcoded in 5 times small icon
	Scale *= 0.1f;

	Canvas->DrawScaledIcon(HealthBarBg, UseMessagePosX, UseMessagePosY - 10.f, FVector::OneVector);

	FCanvasTextItem TextItem1(FVector2D::ZeroVector, FText::FromString("Start Interact"), NormalFont, HUDDark);
	TextItem1.EnableShadow(FLinearColor::Black);
	TextItem1.FontRenderInfo = ShadowedFont;
	FCanvasTextItem TextItem2(FVector2D::ZeroVector, FText::FromString(" (E) "), NormalFont, HUDDark);
	TextItem2.EnableShadow(FLinearColor::Red);
	TextItem2.FontRenderInfo = ShadowedFont;
	TextItem2.SetColor(FColor::Orange);
	FString UseObjectStr = "";
	FCanvasTextItem TextItem3(FVector2D::ZeroVector, FText::FromString("button. " + UseObjectStr), NormalFont, HUDDark);
	TextItem3.EnableShadow(FLinearColor::Black);
	TextItem3.FontRenderInfo = ShadowedFont;

	FVector2D TextUseLen;
	FVector2D TextButtonLen;
	FVector2D TextUseLenFinish;
	Canvas->StrLen(NormalFont, TextItem1.Text.ToString(), TextUseLen.X, TextUseLen.Y);
	Canvas->StrLen(NormalFont, TextItem2.Text.ToString(), TextButtonLen.X, TextButtonLen.Y);
	Canvas->StrLen(NormalFont, TextItem3.Text.ToString(), TextUseLenFinish.X, TextUseLenFinish.Y);
	Canvas->DrawScaledIcon(DialogIcon, UseMessagePosX + OffsetX / 4, UseMessagePosY - 10.f, Scale);
	Canvas->DrawItem(TextItem1, UseMessagePosX + OffsetX, UseMessagePosY);
	Canvas->DrawItem(TextItem2, UseMessagePosX + OffsetX + TextUseLen.X, UseMessagePosY);
	Canvas->DrawItem(TextItem3, UseMessagePosX + OffsetX + TextUseLen.X + TextButtonLen.X, UseMessagePosY);
}

void AOazisShooterHUD::DrawOazisDebugInfoString(const FString& Text, float PosX, float PosY, bool bAlignLeft, bool bAlignTop, const FColor& TextColor)
{
#if !UE_BUILD_SHIPPING
	float ScaleUI = Canvas->ClipY / 1080.0f;
	float SizeX, SizeY;
	Canvas->StrLen(NormalFont, Text, SizeX, SizeY);

	const float UsePosX = bAlignLeft ? PosX : PosX - SizeX;
	const float UsePosY = bAlignTop ? PosY : PosY - SizeY;

	const float BoxPadding = 5.0f;

	FColor DrawColor(HUDDark.R, HUDDark.G, HUDDark.B, HUDDark.A * 0.2f);
	const float X = UsePosX - BoxPadding * ScaleUI;
	const float Y = UsePosY - BoxPadding * ScaleUI;
	// hack in the *2.f scaling for Y since UCanvas::StrLen doesn't take into account newlines
	const float SCALE_Y = 3.0f;

	FCanvasTileItem TileItem(FVector2D(X, Y), FVector2D((SizeX + BoxPadding * SCALE_Y) * ScaleUI, (SizeY * SCALE_Y + BoxPadding * SCALE_Y) * ScaleUI), DrawColor);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);

	FCanvasTextItem TextItem(FVector2D(UsePosX, UsePosY), FText::FromString(Text), NormalFont, TextColor);
	TextItem.EnableShadow(FLinearColor::Black);
	TextItem.FontRenderInfo = ShadowedFont;
	TextItem.Scale = FVector2D(ScaleUI, ScaleUI);
	Canvas->DrawItem(TextItem);
#endif
}

FString AOazisShooterHUD::OazisDateTime(FDateTime Date)
{
	FString DayStr;
	FString MonthStr;

	switch (Date.GetDayOfWeek())
	{
	case EDayOfWeek::Monday:	DayStr = TEXT("Mon");	break;
	case EDayOfWeek::Tuesday:	DayStr = TEXT("Tue");	break;
	case EDayOfWeek::Wednesday:	DayStr = TEXT("Wed");	break;
	case EDayOfWeek::Thursday:	DayStr = TEXT("Thu");	break;
	case EDayOfWeek::Friday:	DayStr = TEXT("Fri");	break;
	case EDayOfWeek::Saturday:	DayStr = TEXT("Sat");	break;
	case EDayOfWeek::Sunday:	DayStr = TEXT("Sun");	break;
	}

	switch (Date.GetMonthOfYear())
	{
	case EMonthOfYear::January:		MonthStr = TEXT("Jan");	break;
	case EMonthOfYear::February:	MonthStr = TEXT("Feb");	break;
	case EMonthOfYear::March:		MonthStr = TEXT("Mar");	break;
	case EMonthOfYear::April:		MonthStr = TEXT("Apr");	break;
	case EMonthOfYear::May:			MonthStr = TEXT("May");	break;
	case EMonthOfYear::June:		MonthStr = TEXT("Jun");	break;
	case EMonthOfYear::July:		MonthStr = TEXT("Jul");	break;
	case EMonthOfYear::August:		MonthStr = TEXT("Aug");	break;
	case EMonthOfYear::September:	MonthStr = TEXT("Sep");	break;
	case EMonthOfYear::October:		MonthStr = TEXT("Oct");	break;
	case EMonthOfYear::November:	MonthStr = TEXT("Nov");	break;
	case EMonthOfYear::December:	MonthStr = TEXT("Dec");	break;
	}
	
	//FString Time = FString::Printf(TEXT("%02i:%02i:%02i"), GetHour(), GetMinute(), GetSecond());
	FString Time = FString::Printf(TEXT("%s %02i:%02i"), *DayStr, Date.GetHour(), Date.GetMinute());
	//return FString::Printf(TEXT("%s, %02d %s %d %s GMT"), *DayStr, GetDay(), *MonthStr, GetYear(), *Time);
	return FString::Printf(TEXT("%02d %s %d %s"), Date.GetDay(), *MonthStr, Date.GetYear(), *Time);
}

void AOazisShooterHUD::BuildRightPanel()
{
	TSharedPtr<SShooterMenuItem> TmpButton;
}

void AOazisShooterHUD::Destroyed()
{
	Super::Destroyed();
	UE_LOG(LogOazisHUD, Log, TEXT("AOazisShooterHUD Destroyed"));
	ShowWorldMapWidget(false);
}

void AOazisShooterHUD::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogOazisHUD, Log, TEXT("AOazisShooterHUD BeginPlay"));
	//UShooterGameInstance* const GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
	ShowWorldMapWidget(CurrentGameMode == EOazisGameMode::WorldMapMode);
	//UE_LOG(LogOazisHUD, Log, TEXT("AOazisShooterHUD CurrentState = %s"), *GameInstance->GetCurrentState().ToString());
}

void AOazisShooterHUD::ShowWorldMapWidget(bool show)
{
	if (GEngine && GEngine->GameViewport)
	{
		if (CurrentGameMode == EOazisGameMode::WorldMapMode) UE_LOG(LogOazisHUD, Log, TEXT("WORLDMAPMODE!"))
		if (CurrentGameMode == EOazisGameMode::ShooterMode) UE_LOG(LogOazisHUD, Log, TEXT("SHOOTERMODE!"))
		if (CurrentGameMode == EOazisGameMode::NoneMode)  UE_LOG(LogOazisHUD, Log, TEXT("NONEMODE!"))
		UGameViewportClient* const GVC = GEngine->GameViewport;
		if (show)
		{
			UE_LOG(LogOazisHUD, Log, TEXT("OazisShooterHUD Add->UI for Worldmap"));
			TestBtnWidget = SNew(STestBtn);
			GVC->AddViewportWidgetContent(TestBtnWidget.ToSharedRef());
		}
		else 
			if (TestBtnWidget)
			{
				UE_LOG(LogOazisHUD, Log, TEXT("OazisShooterHUD Remove->UI for Worldmap"));
				GVC->RemoveViewportWidgetContent(TestBtnWidget.ToSharedRef());
			}
	}
}

void STestBtn::Construct(const FArguments& InArgs)
{
	const FButtonStyle* ButtonStyle = &FShooterStyle::Get().GetWidgetStyle<FButtonStyle>("DefaultShooterButtonStyle");
	
	TSharedRef<FSlateGameResources> SlateResources = FSlateGameResources::New(FName("OazisShooterImage"), "/Game/OazisUI/Styles", "/Game/OazisUI/Styles");
	FSlateGameResources& Style = SlateResources.Get();
	const FSlateBrush* SlateBrush = Style.GetBrush("OazisShooterImage");
	UE_LOG(LogOazisHUD, Log, TEXT("OazisTestBrush ready = %s"), *SlateBrush->GetResourceName().ToString());

	//PlayerOwner = InArgs._PlayerOwner;
	//AOazisShooterPlayerController* MyOazisPC = Cast<AOazisShooterPlayerController>(PC);

	EOazisGameMode eog = EOazisGameMode::WorldMapMode;// MyOazisPC->GetOazisGameMode()->GetCurrentMode(); //EOazisGameMode::WorldMapMode;
	//UE_LOG(LogOazisHUD, Log, TEXT("OazisTestBtn visibility = %d"), GetVisibility(eog));

	//if (SlateBrush) UE_LOG(LogOazisHUD, Log, TEXT("SlateBrush ready = %s"),*SlateBrush->GetResourceName().ToString());

	ChildSlot
		.Padding(8.0f, 2.0f, 0.0f, 0.0f)
		[
			SNew(SHorizontalBox)
			// Find in Content Browser
		+ SHorizontalBox::Slot()
		.VAlign(VAlign_Bottom)
		.Padding(6.0f, 6.0f, 6.0f, 6.0f)
		.AutoWidth()
		[
			SNew(SButton)
			.Text(LOCTEXT("WorldExplore", "EXPLORE (M)"))
		.ToolTipText(LOCTEXT("WorldExploreTip", "Explore local Map on WorldMap point"))
		.ButtonStyle(ButtonStyle)
		.TextStyle(FShooterStyle::Get(), "ShooterGame.MenuTextStyle") //
		.Visibility(GetVisibility(eog)) // EVisibility::Visible
	//.OnClicked(InItem.Get(), NULL)
	//[
		//SNew(STextBlock)
		//.Text(LOCTEXT("WorldExplore", "EXPLORE (M)"))
		//.TextStyle(FShooterStyle::Get(), "ShooterGame.MenuHeaderTextStyle")
	//]
		]
	/* LogoPictures
	    + SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.HAlign(HAlign_Center)
		[
			//Resizes the image
		SNew(SBox)
		.WidthOverride(512)
		.HeightOverride(512)
		[
		SNew(SBorder)
		//.Padding(FMargin(3))
		.BorderImage(SlateBrush)
		//SNew(SImage).Image(FCoreStyle::Get().GetBrush("OazisShooterImage"))
		]
		//.BorderImage(FCoreStyle::Get().GetBrush("OazisShooterImage"))
		//NewSlateWidgetStyleAsset
		]*/
		];
}

#undef LOCTEXT_NAMESPACE
