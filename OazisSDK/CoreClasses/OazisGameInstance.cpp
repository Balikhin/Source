// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "OazisGameInstance.h"
#include "UI/OazisShooterHUD.h"

DEFINE_LOG_CATEGORY(LogOazisGameInstance);

UOazisGameInstance::UOazisGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	//, OnlineMode(EOnlineMode::Online) // Default to online
	//, bIsLicensed(true) // Default to licensed (should have been checked by OS on boot)
{
	EOnlineMode Emod = GetOnlineMode();
	if (Emod == EOnlineMode::Online) UE_LOG(LogOazisGameInstance, Log, TEXT("OazisGameInstance Consctructor init Online"));
}

void UOazisGameInstance::ChangeGameMode(EOazisGameMode GameMode)
{ 
	OazisGameModeInstance = GameMode;
}

EOazisGameMode UOazisGameInstance::GetOazisGameMode()
{
	return OazisGameModeInstance;
}

void UOazisGameInstance::BeginMainMenuState()
{
	Super::BeginMainMenuState();
	ChangeGameMode(EOazisGameMode::NoneMode);
	if (GetWorld()->GetFirstPlayerController())
	{
		Cast<AOazisShooterHUD>(GetWorld()->GetFirstPlayerController()->GetHUD())->ShowWorldMapWidget(false);
		UE_LOG(LogOazisGameInstance, Log, TEXT("OazisGameInstance  Hide Play UI"));
	}
	UE_LOG(LogOazisGameInstance, Log, TEXT("OazisGameInstance  BeginMainMenuState"));
}
