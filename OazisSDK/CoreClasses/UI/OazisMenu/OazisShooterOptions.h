// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "ShooterGame/Private/UI/Menu/Widgets/ShooterMenuItem.h"
#include "ShooterGame/Private/UI/Menu/Widgets/SShooterMenuWidget.h"
#include "ShooterGame/Public/Player/ShooterPersistentUser.h"
#include "ShooterGame/Private/UI/Menu/ShooterOptions.h"


const FIntPoint DefaultShooterResolutions2[] = { FIntPoint(1024,768), FIntPoint(1280,720), FIntPoint(1920,1080) };

const int32 DefaultShooterResCount2 = ARRAY_COUNT(DefaultShooterResolutions2);

/*
DECLARE_DELEGATE(FOnApplyChanges);
*/

class UShooterGameUserSettings;

class OAZISSDK_API FOazisShooterOptions : public TSharedFromThis<FShooterOptions>
{
public:
	/** sets owning player controller */
	void Construct(ULocalPlayer* InPlayerOwner);
	//virtual void Test();
};