﻿// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OazisBuggyPawn.h"
#include "OazisShooterPlayerController.h"
//#include "Bots/ShooterBot.h"
#include "OazisShooterCharacter.h"
//#include "Bots/ShooterAIController.h"

AOazisBuggyPawn::AOazisBuggyPawn(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

/** Dialogue System interface */
bool AOazisBuggyPawn::ModifyIntValue_Implementation(const FName& ValueName, bool bDelta, int32 Value)
{
	if (!DlgData.Integers.Contains(ValueName))
		DlgData.Integers.Add(ValueName, 0);

	if (bDelta)
		DlgData.Integers[ValueName] += Value;
	else
		DlgData.Integers[ValueName] = Value;

	return true;
}

bool AOazisBuggyPawn::ModifyFloatValue_Implementation(const FName& ValueName, bool bDelta, float Value)
{
	if (!DlgData.Floats.Contains(ValueName))
		DlgData.Floats.Add(ValueName, 0.0f);

	if (bDelta)
		DlgData.Floats[ValueName] += Value;
	else
		DlgData.Floats[ValueName] = Value;

	return true;
}

bool AOazisBuggyPawn::ModifyBoolValue_Implementation(const FName& ValueName, bool bValue)
{
	if (bValue)
		DlgData.TrueBools.Add(ValueName);
	else
		DlgData.TrueBools.Remove(ValueName);

	return true;
}

bool AOazisBuggyPawn::ModifyNameValue_Implementation(const FName& ValueName, const FName& NameValue)
{
	if (DlgData.Names.Contains(ValueName))
		DlgData.Names[ValueName] = NameValue;
	else
		DlgData.Names.Add(ValueName, NameValue);

	return true;
}


float AOazisBuggyPawn::GetFloatValue_Implementation(const FName& ValueName) const
{
	return DlgData.Floats.Contains(ValueName) ? DlgData.Floats[ValueName] : 0.0f;
}

int32 AOazisBuggyPawn::GetIntValue_Implementation(const FName& ValueName) const
{
	return DlgData.Integers.Contains(ValueName) ? DlgData.Integers[ValueName] : 0;
}

bool AOazisBuggyPawn::GetBoolValue_Implementation(const FName& ValueName) const
{
	return DlgData.TrueBools.Contains(ValueName);
}

FName AOazisBuggyPawn::GetNameValue_Implementation(const FName& ValueName) const
{
	return DlgData.Names.Contains(ValueName) ? DlgData.Names[ValueName] : NAME_None;
}

void AOazisBuggyPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AOazisBuggyPawn::OnStartUse);
}

void AOazisBuggyPawn::OnStartTalk()
{
	AOazisShooterPlayerController* OazisPlayerController = NULL;
	if (Controller)
	{
		OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
		AActor* TalkActor = OazisPlayerController->ActorForTalk;
		AOazisBuggyPawn* TalkNPCPawn = Cast<AOazisBuggyPawn>(TalkActor);
		FString sNpcName;
		if (TalkNPCPawn)
		{
			TalkNPCPawn->GetName(sNpcName);
			if (GEngine) 
				GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("You try start Talk - " + sNpcName));
			if (TalkNPCPawn->Dialog)
			{
				OazisPlayerController->StartDialogue(TalkNPCPawn->Dialog, TalkNPCPawn);
				if (wWidgetDialog)
				{
					DialogWidget = CreateWidget<UUserWidget>(OazisPlayerController, wWidgetDialog);
					DialogWidget->AddToViewport();
				}
			}
			else
			{
				if (GEngine) 
					GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("NPC - " + sNpcName + " dont have dialogue."));
			}
		}
	}
}

void AOazisBuggyPawn::OnStartUse()
{
	UKismetSystemLibrary::PrintString(GetWorld(), "Use ", 1, 1, FColor::Red, 5);
	PossessBackToOldPawn();
}

void AOazisBuggyPawn::UnPossessed()
{
	Super::UnPossessed();
	EngineAC->Stop(); // Stop sound after exit
}

/**
* Called when this Pawn is possessed. Only called on the server (or in standalone).
*@param C The controller possessing this pawn
*/
void AOazisBuggyPawn::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);

	FString name; GetName(name);
	UKismetSystemLibrary::PrintString(GetWorld(),"Possessed into "+name,1,1,FColor::Green,10); // Using for Separate Client/Server side

	EngineAC->Play();
}

void AOazisBuggyPawn::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (EngineAC)
	{
		EngineAC->SetSound(EngineSound);
		EngineAC->Stop();
	}
}

void AOazisBuggyPawn::SetOazisPlayerPawn(APawn* PawnDriver)
{
	if (PawnDriver)
	OazisPlayerPawn = PawnDriver;
	UKismetSystemLibrary::PrintString(GetWorld(), "Set OazisPlayerPawn - " + OazisPlayerPawn->GetFullName(), 1, 1, FColor::Green, 10);
}

void AOazisBuggyPawn::PossessBackToOldPawn()
{
	if (Role == ROLE_Authority)
		{
			AOazisShooterPlayerController* OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
			if (OazisPlayerController)
			{
				FTransform SpawnLocation = FTransform::Identity;

				SpawnLocation.SetLocation(GetActorLocation() + FVector(0, 0, 700));
				if (OazisPlayerPawn)
				{
					OazisPlayerPawn->SetActorTickEnabled(true);
					OazisPlayerPawn->SetActorHiddenInGame(false);
					OazisPlayerPawn->SetActorRelativeTransform(SpawnLocation);
					OazisPlayerPawn->SetActorEnableCollision(true);
				}
				else return;

				OazisPlayerController->UnPossess();
				//APawn* PlayerPawn = (APawn*) GetWorld()->SpawnActor(AOazisShooterCharacter::StaticClass(),&SpawnLocation);
				OazisPlayerController->Possess(OazisPlayerPawn); //OazisPlayerPawn


			}
		}
		else
		{
			EngineAC->Stop();
			UKismetSystemLibrary::PrintString(GetWorld(), "Stop Engine", 1, 1, FColor::Green, 3);
			ServerPossessBackToOldPawn();
		}
}

bool AOazisBuggyPawn::ServerPossessBackToOldPawn_Validate()
{
	return true;
}

void AOazisBuggyPawn::ServerPossessBackToOldPawn_Implementation()
{
	PossessBackToOldPawn();
}