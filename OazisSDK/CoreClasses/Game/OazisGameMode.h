#pragma once

#include "CoreMinimal.h"
#include "ShooterGameMode.h"
#include "Online/ShooterPlayerState.h"
#include "Online/ShooterGameState.h"
#include "OazisEnums.h"
#include "CoreClasses/UI/OazisShooterHUD.h"
//#include "CoreClasses/OazisGameInstance.h"

#include "OazisGameMode.generated.h"

class AShooterPlayerState;

/*Time Game Management and Calls*/
USTRUCT(BlueprintType)
struct OAZISSDK_API FOazisGameTimeData
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(BlueprintReadWrite, Category = OazisSDK)
	FDateTime DateTimeStartGame = FDateTime(2220, 9, 29, 0); //Start default DateTime game
	UPROPERTY(BlueprintReadWrite, Category = OazisSDK)
	FDateTime DateTimeGame = DateTimeStartGame; //All play in session GameDateTime  The maximum date value is December 31, 9999, 23:59:59.9999999.
	UPROPERTY(BlueprintReadWrite, Category = OazisSDK)
	float GameMinutes;
	UPROPERTY(BlueprintReadWrite, Category = OazisSDK)
	float TimeSpeed = 0.5f; //Default timespeed stay Player
	UPROPERTY(BlueprintReadWrite, Category = OazisSDK)
	int32 GameDays;
	UPROPERTY(BlueprintReadWrite, Category = OazisSDK)
	int DayMinutes = 1440;  //minutes in 1 Day
	/*	FOazisGameTimeData()*/

	void ChangeTimeSpeed(float NewTimeSpeed)
	{
		NewTimeSpeed = FMath::Clamp(NewTimeSpeed, 0.f, 30.f);
		TimeSpeed = NewTimeSpeed;
	};
	int32 GetGameDays() { return GameDays; };
	float GetGameMinutes() { return GameMinutes; };
	float GetTimeSpeed() { return TimeSpeed; };
	void SetTimeSpeed(float speed) { TimeSpeed = speed; };
	void  AddDateTimeGame(float DeltaSeconds)
	{
		GameMinutes += DeltaSeconds * TimeSpeed;
		if (GameMinutes >= DayMinutes)
		{
			GameDays++;
			GameMinutes -= DayMinutes;
		}
		DateTimeGame = DateTimeStartGame + FTimespan(GameDays, 0, GameMinutes, 0);
	};
};

UCLASS()
class OAZISSDK_API AOazisGameMode : public AShooterGameMode //Based on FreeForAll game mode
{
	GENERATED_UCLASS_BODY()
public:
	void StartPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void RestartGame();
	virtual void StartToLeaveMap();
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage);
	virtual void PreInitializeComponents();

	UFUNCTION(BlueprintCallable)
	void ChangeGameMode(EOazisGameMode GameMode);
	UFUNCTION(BlueprintCallable)
	EOazisGameMode GetCurrentMode();
	UFUNCTION(BlueprintCallable)
	TSubclassOf<APawn> GetPawnClassGameMode(EOazisGameMode GameMode);
	
	//~ Begin AActor Interface
	virtual void Tick(float DeltaSeconds) override;
	//~ End AActor Interface
	
	UFUNCTION(BlueprintCallable, Category = "OazisSDK | GameTime")
	FDateTime GetDateTimeGame() { return GameTimeData.DateTimeGame; };
	UFUNCTION(BlueprintCallable, Category = "OazisSDK | GameTime")
	void SetGameTimeSpeed(float timespeed);
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = OazisSDK)
	FOazisGameTimeData GameTimeData; 	/*Time Game Management and Calls*/
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OazisSDK, meta = (AllowPrivateAccess = "true"))
	EOazisGameMode CurrentGameMode = EOazisGameMode::ShooterMode; // Default mode for Hud its Shooter
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OazisSDK)
	TSubclassOf<APawn> WorldMapPawnClass = ShooterPawnClass->StaticClass();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OazisSDK)
	TSubclassOf<APawn> ShooterPawnClass = ShooterPawnClass->StaticClass();

	/** best player */
	UPROPERTY(transient)
		AShooterPlayerState* WinnerPlayerState;

	/** check who won */
	virtual void DetermineMatchWinner() override;
	/** check if PlayerState is a winner */
	virtual bool IsWinner(AShooterPlayerState* PlayerState) const override;
};
