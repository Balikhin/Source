// Fill out your copyright notice in the Description page of Project Settings.

#include "OazisSDK.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OazisSDK, "OazisSDK" );

DEFINE_LOG_CATEGORY(LogOazisGame);