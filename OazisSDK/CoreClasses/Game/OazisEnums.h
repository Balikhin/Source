// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "OazisEnums.generated.h"

// This is the main enum we use to HUD node. Each mode maps to one enum.
UENUM()
enum class EOazisGameMode : uint8
{
	NoneMode,		// No any mode for clear HUD. For CutScene and etc.
	ShooterMode,	// Default Mode for ShooterGame mode
	WorldMapMode,	// Use for WorldMap travels.
};