// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
//Hybrid PAwn class for OazisSDk via plugins 
#pragma once
#include "CoreMinimal.h"
#include "Pawns/BuggyPawn.h"

#include "UserWidget.h"

#include "DlgDialogue.h"
#include "DlgExampleDialogueData.h"
#include "DlgDialogueParticipant.h"

#include "OazisBuggyPawn.generated.h"

UCLASS()
class OAZISSDK_API AOazisBuggyPawn : public ABuggyPawn /*+VehicleGame*/, public IDlgDialogueParticipant /*+DlgSystem*/
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;

	/** setup pawn specific input handlers */
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// Begin Actor overrides
	virtual void PostInitializeComponents() override;
	//virtual void Tick(float DeltaSeconds) override;
	//virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalForce, const FHitResult& Hit) override;
	//virtual void FellOutOfWorld(const UDamageType& dmgType) override;
	// End Actor overrides
	/** [server] perform PlayerState related setup */
	virtual void PossessedBy(class AController* C) override;
	/** Called when our Controller no longer possesses us. */
	virtual void UnPossessed() override;
public:
	void OnStartTalk();
	void OnStartUse();

	/** Dialogue System interface */

	FName GetParticipantName_Implementation() const override { return DlgParticipantName; }
	ETextGender GetParticipantGender_Implementation() const override { return ETextGender::Neuter; }
	FText GetParticipantDisplayName_Implementation(FName ActiveSpeaker) const override { return DlgParticipantDisplayName; }
	UTexture2D* GetParticipantIcon_Implementation(FName ActiveSpeaker, FName ActiveSpeakerState) const override { return DlgParticipantIcon; }

	bool ModifyIntValue_Implementation(const FName& ValueName, bool bDelta, int32 Value) override;
	bool ModifyFloatValue_Implementation(const FName& ValueName, bool bDelta, float Value) override;
	bool ModifyBoolValue_Implementation(const FName& ValueName, bool bValue) override;
	bool ModifyNameValue_Implementation(const FName& ValueName, const FName& NameValue) override;

	float GetFloatValue_Implementation(const FName& ValueName) const override;
	int32 GetIntValue_Implementation(const FName& ValueName) const override;
	bool GetBoolValue_Implementation(const FName& ValueName) const override;
	FName GetNameValue_Implementation(const FName& ValueName) const override;

	bool OnDialogueEvent_Implementation(const FName& EventName) override { return false; }
	bool CheckCondition_Implementation(const FName& ConditionName) const override { return false; }

	/**
	* [server + local] Possess new Pawn
	*
	* @param NewPawn Pawn to possess
	*/
	void PossessBackToOldPawn();
	/** Posses new pawn */
	UFUNCTION(Reliable, Server, WithValidation)
	void ServerPossessBackToOldPawn();

	void SetOazisPlayerPawn(APawn* PawnDriver);
private:
	APawn * OazisPlayerPawn = nullptr; //Main Player Character siting in Car
	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		//UTexture2D* FactionIcon; // If will be have factions in the game Oazis
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<UUserWidget> wWidgetDialog;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		UDlgDialogue* Dialog;
	/** Dialogue System Properties */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		FDlgExampleDialogueData DlgData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		FName DlgParticipantName = FName("MyCharacterName");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		FText DlgParticipantDisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DlgData, meta = (AllowPrivateAccess = "true"))
		UTexture2D* DlgParticipantIcon;
protected:
	/** The widget we use to display if the node has voice/sound variables set */
	UUserWidget* DialogWidget;
};

