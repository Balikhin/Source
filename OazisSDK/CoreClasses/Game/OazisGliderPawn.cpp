// Fill out your copyright notice in the Description page of Project Settings.

#include "OazisGliderPawn.h"
#include "Kismet/KismetSystemLibrary.h"
#include "OazisShooterPlayerController.h"
//#include "Engine.h"
// Sets default values
AOazisGliderPawn::AOazisGliderPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AOazisGliderPawn::OnStartUse()
{
	UKismetSystemLibrary::PrintString(GetWorld(), "Use ", 1, 1, FColor::Red, 5);
	PossessBackToOldPawn();
}

void AOazisGliderPawn::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);
	bCanFly = true;
	FString name; GetName(name);
	//UE_LOG(LogTemp, Log, TEXT("Possessed"));
	UKismetSystemLibrary::PrintString(GetWorld(), "Possessed into " + name, 1, 1, FColor::Green, 10); // Using for Separate Client/Server side
}

void AOazisGliderPawn::UnPossessed()
{
	Super::UnPossessed();
	bCanFly = false;
}

void AOazisGliderPawn::SetOazisPlayerPawn(APawn* PawnDriver)
{
	if (PawnDriver)
	OazisPlayerPawn = PawnDriver;
	UKismetSystemLibrary::PrintString(GetWorld(), "Set OazisPlayerPawn - " + OazisPlayerPawn->GetFullName(), 1, 1, FColor::Green, 10);
}

void AOazisGliderPawn::PossessBackToOldPawn()
{
	if (Role == ROLE_Authority)
	{
		AOazisShooterPlayerController* OazisPlayerController = Cast<AOazisShooterPlayerController>(Controller);
		if (OazisPlayerController)
		{
			FTransform SpawnLocation = FTransform::Identity;

			SpawnLocation.SetLocation(GetActorLocation() - GetActorForwardVector()* 400);
			if (OazisPlayerPawn)
			{
				OazisPlayerPawn->SetActorTickEnabled(true);
				OazisPlayerPawn->SetActorHiddenInGame(false);
				OazisPlayerPawn->SetActorRelativeTransform(SpawnLocation);
				OazisPlayerPawn->SetActorRotation(FRotator(0, GetActorRotation().Yaw, 0), ETeleportType::ResetPhysics);
				OazisPlayerPawn->SetActorEnableCollision(true);
			}
			else return;

			OazisPlayerController->UnPossess();
			//APawn* PlayerPawn = (APawn*) GetWorld()->SpawnActor(AOazisShooterCharacter::StaticClass(),&SpawnLocation);
			OazisPlayerController->Possess(OazisPlayerPawn); //OazisPlayerPawn
			bCanFly = false;

		}
	}
	else
	{
		ServerPossessBackToOldPawn();
	}
}

bool AOazisGliderPawn::ServerPossessBackToOldPawn_Validate()
{
	return true;
}

void AOazisGliderPawn::ServerPossessBackToOldPawn_Implementation()
{
	PossessBackToOldPawn();
}



// Called when the game starts or when spawned
void AOazisGliderPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOazisGliderPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AOazisGliderPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AOazisGliderPawn::OnStartUse);
}

